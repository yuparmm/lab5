import math
import random
# print("Завдання 1: Користувач вводить дві сторони трьох прямокутників. Вивести їх площі.")
# def ploshcha(a, b):
#     return a * b
# for i in range(3):
#     try:
#         a = float(input("A = "))
#         b = float(input("B = "))
#         print("Площа =", ploshcha(a, b))
#     except ValueError:
#         print("Потрібно ввести числа. Спробуйте ще раз.")






# print("Завдання 2: Дано катети двох прямокутних трикутників. Написати функцію обчислення довжини\nгіпотенузи цих трикутників. Порівняти і вивести яка з гіпотенуз більше, а яка менше.")
# def hipotenuza(A,B):
#     return math.sqrt(A**2+B**2)
# try:
#     A = float(input("Катет 1: "))
#     B = float(input("Катет 2: "))
#     hipotenuza1 = hipotenuza(A,B)
#     A = float(input("Катет 1: "))
#     B = float(input("Катет 2: "))
#     hipotenuza2 = hipotenuza(A,B)
#     if hipotenuza1 > hipotenuza2:
#         print("Гіпотенуза 1: ", hipotenuza1," > ","Гіпотенуза 2: ",hipotenuza2)
#     elif hipotenuza2 > hipotenuza1:
#         print("Гіпотенуза 1: ", hipotenuza1," < ","Гіпотенуза 2: ",hipotenuza2)
#     else:
#         print("Гіпотенузи рівні між собою!")
# except ValueError:
#     print("Введено некоректні дані. Спробуйте ще раз, вводячи лише числа.")


# print("Завдання 3: Задано коло (x-a)2 + (y-b)2 = R2 і точки Р (р1, р2), F (f1, f1), L (l1, l2). \nЗ'ясувати і вивести на екран, скільки точок лежить всередині кола. Перевірку, чи лежить точка всередині кола, \nоформити у вигляді функції.")
# def isinside(x, y, a, b, r):
#     return (x - a)**2 + (y - b)**2 <= r**2
# try:
#     a = float(input("Введіть координату x центру кола: "))
#     b = float(input("Введіть координату y центру кола: "))
#     r = float(input("Введіть радіус кола: "))
#     p1 = float(input("Введіть координату x точки P: "))
#     p2 = float(input("Введіть координату y точки P: "))
#     f1 = float(input("Введіть координату x точки F: "))
#     f2 = float(input("Введіть координату y точки F: "))
#     l1 = float(input("Введіть координату x точки L: "))
#     l2 = float(input("Введіть координату y точки L: "))
# except ValueError:
#     print("Введено некоректні дані. Спробуйте ще раз, вводячи лише числа.")
# else:
#     ifPinside = isinside(p1, p2, a, b, r)
#     ifFinside = isinside(f1, f2, a, b, r)
#     ifLinside = isinside(l1, l2, a, b, r)
#     print("Точка P:", "всередині" if ifPinside else "зовні")
#     print("Точка F:", "всередині" if ifFinside else "зовні")
#     print("Точка L:", "всередині" if ifLinside else "зовні")



# print("Завдання 4: Дано числа X, Y, Z, Т - довжини сторін чотирикутника. Обчислити його площу, якщо кут між сторонами довжиною X і Y - прямий. ")
# # площа прямокутного трикутника
# def ploshcha1(X, Y):
#     return X * Y * 0.5
# # формула Герона
# def ploshcha2(Z, T, d):
#     p = (Z + T + d) / 2
#     return math.sqrt(p * (p - Z) * (p - T) * (p - d))
# try:
#     X = float(input(f"Введіть 1 сторону чотирикутника: "))
#     Y = float(input(f"Введіть 2 сторону чотирикутника: "))
#     Z = float(input(f"Введіть 3 сторону чотирикутника: "))
#     T = float(input(f"Введіть 4 сторону чотирикутника: "))
#     # діагональ
#     d = math.sqrt(X**2 + Y**2)
#     S = ploshcha1(X, Y) + ploshcha2(Z, T, d)
#     print("Площа чотирикутника:", S)
# except ValueError:
#     print("Було введено некоректні дані. Спробуйте ще раз, вводячи лише числа.")






# print("Завдання 5: Знайти всі натуральні числа, що не перевищують заданого n, які діляться на кожне із заданих користувачем чисел.")
# def numbers(n, divisors):
#     lst = []
#     for i in range(1, n + 1):
#         if all(i % divisor == 0 for divisor in divisors):
#             lst.append(i)
#     return lst
# while True:
#     try:
#         n = int(input("Введіть верхню межу діапазона (натуральне число): "))
#         if n <= 0:
#             print("Потрібно ввести натуральне число, більше за 0. Спробуйте ще раз.")
#             continue
#         break
#     except ValueError:
#         print("Це не число. Спробуйте ще раз.")
# while True:
#     try:
#         divisors_input = input("Введіть через пробіл числа, на які мають ділитись числа: ")
#         divisors = [int(i) for i in divisors_input.split()]
#         if any(divisor <= 0 for divisor in divisors):
#             print("Всі числа мають бути натуральними (більше 0). Спробуйте ще раз.")
#             continue
#         break
#     except ValueError:
#         print("Було введено не лише числа. Спробуйте ввести числа ще раз.")
# print(numbers(n, divisors))







# print("Завдання 6: Скласти програму для знаходження чисел з інтервалу [М, N], що мають найбільшу кількість дільників.")
# def MaxCount(t):
#   temp = 0
#   for i in range(1, t + 1):
#     if t % i == 0:
#       temp += 1
#   return temp
# M = int(input("M= "))
# N = int(input("N= "))
# kilk = 0
# chislo = 0
# temp = 0
# for i in range(M, N + 1):
#   temp = MaxCount(i)
#   if temp > kilk:
#     kilk = temp
#     chislo = i
# print(f"Максимальна кількість {kilk}")
# print(f"Число {chislo}")






# print("Завдання 7: Написати функцію для пошуку всіх простих чисел від 0 до N з можливістю вибору \nформату представлення результату (списком; рядками в стовпчик; просто вивести кількість простих чисел.")
# def proste(x):
#   if x < 2:
#     return False
#   for i in range(2, x):
#     if x % i == 0:
#       return False
#   return True
# N = int(input("N= "))
# arr = []
# for i in range(2, N + 1):
#   if proste(i):
#     arr.append(i)
# print("1-Список, 2-Стовпчик, 3-Кількість")
# t = int(input())
# if t==1:
#     print(f"Числа {arr}")
# elif t==2:
#     for i in range(0,len(arr)):
#         print(arr[i])
# elif t==3:
#     print(len(arr))




# print("Завдання 8")
# def second_list(size, rand_max, rand_min, upper, bottom):
#     rand_list = [random.randint(rand_min, rand_max) for i in range(size)]
#     try:
#         if bottom < 0:
#             raise ValueError("Помилка введення")
#         if upper < 0:
#             raise ValueError("Помилка введення.")
#         min_res = min(rand_list) + bottom
#         max_res = max(rand_list) - upper
#         if min_res >= max_res:
#             raise ValueError("Помилка")
#         res_list = [i for i in rand_list if min_res <= i <= max_res]
#     except Exception as e:
#         print("Помилка")
#         print(e)
#         return [], []
#     return rand_list, res_list
# try:
#     N = int(input("Введіть кількість значень "))
#     if N <= 0:
#         raise ValueError("Помилка")
#     min_val = int(input("Введіть мінімальне значення "))
#     max_val = int(input("Введіть максимальне значення "))
#     bottom = int(input("Bottom: "))
#     upper = int(input("Upper: "))
#     res = second_list(N, max_val, min_val, upper, bottom)
#     print(f"\n1: {res[0]}\n2: {res[1]}")
# except ValueError as ve:
#     print(ve)




















